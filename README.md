# Studio51 ReadMe #

A complete redesign of my portfolio website, with the purpose of teaching myself how to make AngularJS applications.

## Utilized##

* Gulp
* SASS
* AngularJS
* WordPress for admin purposes (not included)

## Instructions##

* Run npm install to get all dependencies
* Install WordPress in subdirectory wp