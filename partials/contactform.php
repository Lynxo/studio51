<div ng-controller="ContactForm">
	<form name="contactForm" class="contact-form" novalidate>
		
		<p ng-show="success" class="contact-form__success">Your message has been sent. Thanks!</p>
		<p ng-show="error" class="contact-form__error">Something went wrong, please try again.</p>
	
		<!-- NAME -->
		<p>
			<input type="text" name="name" ng-model="formData.name" placeholder="* Name" required="" />
		</p>
		<!-- EMAIL -->
		<p>
			<input type="email" name="email" ng-model="formData.email" placeholder="* Email" required />
		</p>
		<!-- MESSAGE -->
		<p>
			<textarea name="message" ng-model="formData.message" placeholder="* Message" required></textarea>
		</p>
		<!-- SUBMIT BUTTON -->
		<input type="submit" value="submit" ng-click="processForm()" ng-disabled="contactForm.$invalid" ng-class="{disabled:contactForm.$invalid}" value="Send" />
	</form>
</div>