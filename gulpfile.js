var gulp = require('gulp');
var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');

gulp.task('default', ['watch']);

gulp.task('watch', function(){
	gulp.watch('scss/**/*.scss', ['build', 'css-minify']);
})



gulp.task('build', function() {
	return gulp.src('scss/**/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('css'));
		
	
});

gulp.task('css-minify', ['build'], function() {
	return gulp.src('css/style.css')
        .pipe(cssnano())
        .pipe(gulp.dest('css'));
});

