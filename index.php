<?php
$localhost_list = array(
    '127.0.0.1'
);
?>
<!doctype html>
<html  ng-app="studio51App">
	<head>
		<?php if(!in_array($_SERVER['REMOTE_ADDR'], $localhost_list)){ ?>
			<base href="/">
		<?php }else{ ?>
			<base href="/Studio51/">
		<?php } ?> 
		
		<meta charset="UTF-8">
		<title>Studio51</title>
		
		<meta name="fragment" content="!">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700" />
		<link rel="stylesheet" href="css/style.css" />
		
		<script type='text/javascript' src='js/angular/angular.min.js'></script>
		<script type='text/javascript' src='js/angular/angular-animate.min.js'></script>
		<script type='text/javascript' src='js/angular/angular-route.min.js'></script>
		<script type='text/javascript' src='js/angular/angular-sanitize.min.js'></script>
		<script type='text/javascript' src='js/studio51app.js'></script>
		<script type='text/javascript' src='js/WPService.js'></script>

	</head>
	<body>
		
		<div class="wrapper">
			
			<section class="site_headers" ng-controller="SiteHeaders">
				<h1><a href="/" ng-bind="siteTitle"></a></h1>
				<h2 ng-bind="siteTagline"></h2>
			</section>
			
			<aside class="main-nav">
				
				<nav class="main-nav__menu" role="navigation" ng-controller="Menu">
					
					<ul>
						<li ng-repeat="post in posts" active-link>
							<a href="{{post.link}}" data-id="{{post.id}}">{{ post.title.rendered }}</a>
						</li>
					</ul>
					
				</nav>
				
			</aside>
			
			<section class="main-content">
				
				<div ng-view></div>
				
			</section>
			
		</div>
		
	</body>
</html>