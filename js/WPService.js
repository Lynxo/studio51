function WPService($http) {

	var WPService = {
		pages: [],
		pageTitle : ''
	};
	
	WPService.getMenu = function() {
		return $http({
			method: 'GET',
			url: 'wp/wp-json/wp/v2/pages',
			params: {
				'filter[posts_per_page]' : -1,
				'order' : 'asc',
				'orderby' : 'date',
				'context' : 'embed'
			}
			
		});
	}

	WPService.getPage = function(name) {
		if(name === undefined) { name = 'home' };
		return $http({
			method: 'GET',
			url: 'wp/wp-json/wp/v2/pages/',
			params: {
				'filter[name]' : name
			}
		});
	};
	
	WPService.updateTitle = function(title) {
		document.querySelector('title').innerHTML = title + ' | Studio51';
	}

	return WPService;
}

studio51App.factory('WPService', ['$http', WPService]);