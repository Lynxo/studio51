var studio51App = angular.module( 'studio51App', ['ngRoute', 'ngAnimate', 'ngSanitize']  );

studio51App.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
	 
	$routeProvider
	.when('/', {
		templateUrl: 'partials/start.html',
		controller: 'Content'
	})
	.when('/:link', {
		templateUrl: 'partials/content.html',
		controller: 'Content'
	})
	
	.otherwise({
		redirectTo: '/'
	});
	
	$locationProvider.html5Mode(true);
}]);

//===============DIRECTIVES===============//

//Active link menu directive
studio51App.directive('activeLink', ['$location', function($location) {
	var link = function(scope, element, attrs) {
		scope.$watch(function() { return $location.path(); }, 
		function(path) {
			var url = element.find('a').attr('href');
			
			//Overly complicated, needs to be cleaned up
			url = url.split('/');
			url = url[url.length-2];
			if(url == 'www.studio51.se') { url = '/' }
			if(url == 'Studio51') { url = '/' }
			if(path !== '/') {
				path = path.replace('/', '');
				path = path.replace('/', '');
			}
			
			if (path == url) {
				element.addClass("current-menu-item");
			} else {
				element.removeClass('current-menu-item');
			}
		
		});
	};
	
	return {
		restrict: 'A',
		link: link
	};
}]);

//Active link menu directive
studio51App.directive('loading', ['$location', function($location) {
	return {
		restrict: 'E',
		template: '<div class="sk-folding-cube"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div>'
	}
}]);


//===============CONTROLLERS===============//

// Site header
studio51App.controller('SiteHeaders', ['$scope', '$http', function($scope, $http) {
	//Abandoned for now
	//JSON errors in the response. Bug?
	/*$http({
		method: 'GET',
		url: 'wp/wp-json'
	})
	.success( function(data, status, headers, config) {
		$scope.siteTitle = data['name'];
	});*/
	$scope.siteTitle = 'Studio51';
	$scope.siteTagline = 'Portfolio of a front end programmer';
}]);

//Menu controller
studio51App.controller('Menu', ['$scope', '$http', 'WPService', function($scope, $http, WPService) {
	
	WPService.getMenu().success( function( data ) {
		for(var i=0; i < data.length; i++) {
			data[i]['link'] = data[i]['link'].replace('wp/', '');
		}
		
		$scope.posts = data;

	})
}]);

//Content controller
studio51App.controller('Content', ['$scope', '$routeParams', '$location', 'WPService', function($scope, $routeParams, $location, WPService) {
	$scope.isLoading = true;
	WPService.getPage($routeParams.link).success(function (data) {
		if (typeof data !== 'undefined' && data.length > 0) {
			WPService.page = data[0];
			WPService.updateTitle(data[0].title.rendered);
			
			if((data[0].title.rendered) === 'Contact') {
				$scope.contactInclude = 'partials/contactform.php'
			}
			
		}else{
			WPService.is404 = true;
			WPService.updateTitle('Page not found');
		}
		$scope.isLoading = false;
		angular.element(document.querySelector('.main-content__content')).addClass('active');
		
	});
	
		
	$scope.data = WPService;
}]);

//Contact form controller
studio51App.controller('ContactForm', ['$scope', '$http', function($scope, $http) {
	$scope.formData = {};
	$scope.processForm = function() {
		$http({
			method  : 'POST',
			url     : 'partials/contactform-process.php',
			data    : $scope.formData,
			headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).success( function( data, status, headers, config ) {
			if ( data.success ) {
				$scope.success = true;
			} else {
				$scope.error = true;
			}
		});
	};
}]);